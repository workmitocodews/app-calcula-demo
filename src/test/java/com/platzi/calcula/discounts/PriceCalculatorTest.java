package com.platzi.calcula.discounts;

import org.apache.log4j.Logger;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PriceCalculatorTest {
    private final static Logger log = Logger.getLogger(PriceCalculatorTest.class);

    @Test
    public void PriceCalculator_TotalCero() {

        PriceCalculator calculator = new PriceCalculator();
        assertThat( calculator.getTotal(), is(0.0) );
    }

    @Test
    public void PriceCalculator_SumaPrecios() {

        PriceCalculator calculator = new PriceCalculator();

        calculator.addPrice(10.2);
        calculator.addPrice(15.5);

        assertThat( calculator.getTotal(), is(25.7) );
    }

    @Test
    public void PriceCalculator_AplicarDescuento(){

        log.info("Test: Cálculo de descuentos");
        PriceCalculator calculator = new PriceCalculator();

        log.info("Monto a agregar: "+100);
        log.info("Monto a agregar: "+50);
        log.info("Monto a agregar: "+50);
        calculator.addPrice(100);
        calculator.addPrice(50);
        calculator.addPrice(50);

        log.info("Monto de descuento: "+25);
        calculator.setDiscount(25);

        log.info("Monto total: "+150);
        assertThat( calculator.getTotal(), is(150.0) );
    }

}
